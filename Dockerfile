FROM node:18

RUN apt-get update && apt-get upgrade && npm i -g pm2
WORKDIR /app

COPY . .
RUN npm i && npm run compile
EXPOSE 3000

CMD ["pm2-runtime", "buid/src/index.js"]