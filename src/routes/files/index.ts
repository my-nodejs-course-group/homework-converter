import { Router } from 'express';
import { multerSettings } from './controllers/uploadFile/multer.settings';
import { timeLimitsController } from './controllers/uploadFile/time-limits.controller';
import { checkFilesExtController } from './controllers/uploadFile/check-files-ext.controller';
import { checkConvertionsCounterController } from './controllers/uploadFile/check-convertions-counter.controller';
import { uploadFileController } from './controllers/uploadFile/files.controller';
import { checkFilesController } from './controllers/check-files.controller';
import { checkStatusController } from './controllers/check-status.controller';
import { downloadController } from './controllers/download.controller';
import { getSingleController } from './controllers/get-single.controller';
import { buyConvertationsController } from './controllers/buy-convertations.controller';
import { checkIsAuthController } from './controllers/check-is-auth.controller';

const filesRoutes = Router();

filesRoutes.post(
  '/upload',
  multerSettings.array('photos'),
  timeLimitsController,
  checkFilesExtController,
  checkConvertionsCounterController,
  uploadFileController
);

filesRoutes.post('/check-files', checkFilesController); // just for testing
filesRoutes.post('/check-status', checkStatusController);
filesRoutes.post('/download', downloadController);
filesRoutes.get('/:name', getSingleController);
filesRoutes.post('/buy', checkIsAuthController, buyConvertationsController);

export { filesRoutes };
