import { Request, Response } from 'express';
import { checkUserStorage } from 'storage';

export const checkFilesController = (req: Request, res: Response) => {
  res.json({
    status: 'ok',
    files: checkUserStorage(),
  });
};
