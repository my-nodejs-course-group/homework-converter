import { Request, Response } from 'express';
import { getFilePath } from 'storage';

export const downloadController = (req: Request, res: Response) => {
  const fileData = getFilePath(req.body.userId, req.body.fileId);

  res.download(fileData.path, fileData.name, (err) => {
    if (err) {
      res.status(500).send({
        message: 'Could not download the file. ' + err,
      });
    }
  });
};
