import { Request, Response } from 'express';
import { getFilesStatuses } from 'storage';

export const checkStatusController = (req: Request, res: Response) => {
  res.json({
    status: 'ok',
    files: getFilesStatuses(req.body.userId),
  });
};
