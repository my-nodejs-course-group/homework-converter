import { Request, Response } from 'express';
import { setExtraConvertations } from 'storage';
export const buyConvertationsController = (req: Request, res: Response) => {
  setExtraConvertations(req.userData.id, 200);
  res.json({
    status: 'ok'
  })
};
