import { Request, Response } from 'express';
import path from 'path';

export const getSingleController = (req: Request, res: Response) => {
  res.download(
    path.join(process.cwd(), '/tempFiles/converted', req.params.name),
    req.params.name,
    (err) => {
      if (err) {
        res.status(500).send({
          message: "Could not download the file. " + err,
        });
      }
    }
  );
};
