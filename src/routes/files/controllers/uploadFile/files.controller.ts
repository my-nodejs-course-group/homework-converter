import { Request, Response } from 'express';
import { addFilesToUser, isPaidConvertation } from 'storage';
import { FileStatus, FilesData } from 'storage/types';
import crypto from 'crypto';
import path from 'path';
import { userQueue } from 'fileProcessor/userQueue';
import { fileProcessor, FileProcessor } from 'fileProcessor/fileProcessor';
import { Queue } from 'queue/queue';


export const uploadFileController = async (req: Request, res: Response) => {
  const userData: FilesData = {};
  (req.files as Express.Multer.File[])?.forEach(file => {
    const fileId = crypto.randomBytes(16).toString('hex');
    const dirPathOut = path.join(path.dirname(file.destination), 'converted');
    const filePathOut = path.format({
      dir: dirPathOut,
      base: `${path.parse(file.filename).name}.webp`,
    });
    userData[fileId] = {
      filePathIn: file.path,
      filePathOut,
      originalName: file.originalname,
      status: FileStatus.pending,
    };
  });

  addFilesToUser(userData, req.userData.id);
  if (isPaidConvertation(req.userData.id)) {
    const paidQueue = new Queue<string>();
    paidQueue.push(req.userData.id);
    const paidConverter = new FileProcessor(paidQueue);
    paidConverter.startProcessing();
  } else {
    userQueue.push(req.userData.id);
    if(userQueue.getSize() === 1) fileProcessor.startProcessing();
  }

  res.json({
    status: 'ok',
  });
};
