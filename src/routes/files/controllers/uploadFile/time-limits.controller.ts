import { NextFunction, Request, Response } from 'express';
import moment from 'moment';

import { checkUserStorage, setTimeTrack, updateCounter } from 'storage';

export const timeLimitsController = (req: Request, res: Response, next: NextFunction) => {
  let diff = null;
  if (!checkUserStorage()[req.userData.id].timeTracker) { // first upload for this user
    setTimeTrack(moment().utc(), req.userData.id);
    updateCounter(req.userData.id, req.userData.isAuth);
    next();
    return;
  }
  diff = moment().utc().diff(checkUserStorage()[req.userData.id].timeTracker, 'seconds')
  if (req.userData.isAuth) {
    if (diff as number > Number(process.env.AUTH_TIME_LIMIT)) {
      setTimeTrack(moment().utc(), req.userData.id);
      updateCounter(req.userData.id, true);
    }
  } else if (diff as number > Number(process.env.NOT_AUTH_TIME_LIMIT)) {
    setTimeTrack(moment().utc(), req.userData.id);
    updateCounter(req.userData.id, false);
  }
  next();
};
