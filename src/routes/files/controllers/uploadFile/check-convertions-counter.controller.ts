import { Request, Response, NextFunction } from 'express';
import { decrimentFilesCounter, userHasEnoughConvertations } from 'storage';

export const checkConvertionsCounterController = (req: Request, res: Response, next: NextFunction) => {
  const filesLength = (req.files as Express.Multer.File[]).length;
  if (userHasEnoughConvertations(req.userData.id, filesLength)) {
    decrimentFilesCounter(req.userData.id, filesLength);
    next();
  } else {
    res.json({
      error: 'max files in your period converted'
    });
  }
};

