import { Request, Response } from 'express';
import { AccessData } from 'storage/types';

export const googleCallBackController = (req: Request, res: Response) => {
    const accessData = <AccessData>req.user
    res.cookie('accessToken', accessData.accessToken, { httpOnly: true });
    res.cookie('refreshToken', accessData.refreshToken, { httpOnly: true });
    res.redirect('/health');
};
