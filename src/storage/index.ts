import { UsersStorage, FileStatus, FilesStatuses, AccessData, FilesData } from './types';
import path from 'path';
import { Moment } from 'moment';

const usersStorage: UsersStorage = {};

export const addFilesToUser = (
  filesData: FilesData,
  userId: string
) => {
  usersStorage[userId].files = {
    ...usersStorage[userId].files,
    ...filesData,
  };
};

export const checkUserStorage = () => {
  return { ...usersStorage };
};

export const getFilesStatuses = (userId: string): FilesStatuses | null => {
  if (!usersStorage[userId]) {
    return null;
  }
  return Object.keys(usersStorage[userId].files).map(fileId => {
    const cleanFileName = path.parse(usersStorage[userId].files[fileId].filePathOut).base;
    return {
      fileId,
      status: usersStorage[userId].files[fileId].status,
      fileName: usersStorage[userId].files[fileId].originalName,
      downloadLink: `http://localhost:3000/files/${cleanFileName}`
     }
  })
};

export const getFilePath = (userId: string, fileId: string): { path: string, name: string } => {
  return { path: usersStorage[userId].files[fileId].filePathOut, name: usersStorage[userId].files[fileId].originalName };
}

export const changeFilesStatus = (
  userId: string,
  fileId: string,
  newStatus: FileStatus
) => {
  usersStorage[userId].files[fileId].status = newStatus;
};

export const setFilePathOut = (
  userId: string,
  fileId: string,
  filePathOut: string
) => {
  usersStorage[userId].files[fileId].filePathOut = filePathOut;
};

export const decrimentFilesCounter = (userId: string, filesCount: number) => {
  if (usersStorage[userId].accessData.extraConvertsCounter > 0) {
    usersStorage[userId].accessData.isPaid = true;
    usersStorage[userId].accessData.extraConvertsCounter-=filesCount;
    if (usersStorage[userId].accessData.extraConvertsCounter < 0) {
      usersStorage[userId].accessData.filesCounter += usersStorage[userId].accessData.extraConvertsCounter;
      usersStorage[userId].accessData.extraConvertsCounter = 0;
    }
  } else {
    usersStorage[userId].accessData.isPaid = false;
    usersStorage[userId].accessData.filesCounter-=filesCount;
  }
};

export const userHasEnoughConvertations = (userId: string, filesCount: number) => {
  return usersStorage[userId].accessData.filesCounter + usersStorage[userId].accessData.extraConvertsCounter >= filesCount
};

export const isUserExist = (userId: string) => Object.keys(usersStorage).includes(userId);

export const createUserInStorage = (userId: string, userAccessData: AccessData) => {
  usersStorage[userId] = {
    files: {},
    accessData: { ...userAccessData }
  };
};

export const getUserAccessData = (userId: string) => {
  if (Object.keys(usersStorage).includes(userId)) {
    return usersStorage[userId].accessData;
  }
  return undefined;
};

export const setTimeTrack = (time: Moment, userId: string) => {
  usersStorage[userId].timeTracker = time;
}

export const updateCounter = (userId: string, isAuth: boolean) => {
  if (usersStorage[userId]?.accessData?.filesCounter !== undefined) {
    if (isAuth) {
      (usersStorage[userId].accessData as AccessData).filesCounter = Number(process.env.MAX_AUTH_CONVERTS);
    } else {
      (usersStorage[userId].accessData as AccessData).filesCounter = Number(process.env.MAX_NOT_AUTH_CONVERTS);
    }
  }
}

export const setExtraConvertations = (userId: string, extraCount: number) => {
  usersStorage[userId].accessData.extraConvertsCounter = extraCount;
};

export const isPaidConvertation = (userId: string) => {
  return usersStorage[userId].accessData.isPaid;
};
