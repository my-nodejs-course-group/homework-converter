import { FileData } from 'storage/types';

export interface FileQueueItem {
  fileId: string;
  userId: string;
  fileData: FileData;
}
