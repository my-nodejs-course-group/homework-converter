import { Converter } from '../converter/converter';
import { Queue } from '../queue/queue';
import { changeFilesStatus, checkUserStorage } from 'storage';
import { FileData, FileStatus, UserData } from 'storage/types';
import { FileQueueItem } from './fileProcessor.model';
import { userQueue } from './userQueue';

export class FileProcessor {
  private fileQueue_1: Queue<FileQueueItem>;
  private fileQueue_2: Queue<FileQueueItem>;
  private fileQueue_3: Queue<FileQueueItem>;

  private userQueue: Queue<string>;
  private converter: Converter;

  constructor(userQueue: Queue<string>) {
    this.fileQueue_1 = new Queue();
    this.fileQueue_2 = new Queue();
    this.fileQueue_3 = new Queue();
    this.converter = new Converter();
    this.userQueue = userQueue;
  }

  public startProcessing = async () => {
    const queueArray = [this.fileQueue_1, this.fileQueue_2, this.fileQueue_3];
    queueArray.forEach(queueItem => {
      this.setFilesInOneQueue(queueItem);
    });
  };

  private setFilesInOneQueue = (queueItem: Queue<FileQueueItem>) => {
    if (queueItem.getSize() === 0 && this.userQueue.getSize() > 0) {
      const userId = this.userQueue.pop();
      if (userId) {
        const filesObj = checkUserStorage()[userId].files;
        this.setUserFilesInQueue(filesObj, userId, queueItem);
        this.convertFiles(queueItem);
      }
    }
  };

  private setUserFilesInQueue = (
    filesObj: UserData['files'],
    userId: string,
    fileQueue: Queue<FileQueueItem>
  ) => {
    const filesArr = Object.values(filesObj);
    const filesIds = Object.keys(filesObj);
    filesArr.forEach((file: FileData, idx) => {
      fileQueue.push({
        fileId: filesIds[idx],
        userId,
        fileData: file,
      });
    });
  };

  private convertFiles = (fileQueue: Queue<FileQueueItem>) => {
    const fileItem = fileQueue.pop();

    if (fileItem) {
      changeFilesStatus(fileItem.userId, fileItem.fileId, FileStatus.pending);
      this.converter
        .convertFile(fileItem)
        .then(() => {
          changeFilesStatus(fileItem.userId, fileItem.fileId, FileStatus.ready);
        })
        .catch(error => {
          console.error(error);
          changeFilesStatus(fileItem.userId, fileItem.fileId, FileStatus.error);
        })
        .finally(() => {
          if (fileQueue.getSize() > 0) {
            this.convertFiles(fileQueue);
          } else if (
            fileQueue.getSize() === 0 &&
            this.userQueue.getSize() > 0
          ) {
            this.setFilesInOneQueue(fileQueue);
            this.convertFiles(fileQueue);
          } else {
            console.log('File and user Queues is empty!');
          }
        });
    }
  };
}

export const fileProcessor = new FileProcessor(userQueue);
