import path from 'path';
import sharp from 'sharp';
import { FileQueueItem } from '../fileProcessor/fileProcessor.model';

export class Converter {
  private converter: any;
  private fileExtantions: string[];

  constructor() {
    this.converter = sharp;
    this.fileExtantions = ['.jpg', '.png', '.jpeg'];
  }

  public convertFile = async (
    fileItem: FileQueueItem,
    quality?: number
  ): Promise<void> => {
    const ext = path.extname(fileItem.fileData.filePathIn);
    if (this.fileExtantions.includes(ext)) {
      return this.converter(fileItem.fileData.filePathIn)
        .webp({ quality: quality ?? 80 })
        .toFile(fileItem.fileData.filePathOut);
    }
  };
}
