import session from 'express-session';
import { AccessData } from 'storage/types';

declare module 'express-session' {
   interface SessionData {
      user: AccessData;
     }
}
declare global {
  namespace Express {
    interface Request {
      userData: AccessData;
    }
    interface User {
      id: string
    }
  }
  // namespace Express {
  //   interface User extends AccessData {
  //     // A basic field so EsLint does not think it's an empty interface,
  //     // and replace it with some other code which it thinks to be better
  //     // (cause for me, it was replacing automatically the line with
  //     // "type User = UserDocument" which was making Typescript unhappy).
  //     // This field is useless, but that's the only solution I found.
  //     uselessField?: boolean;
  //   }
  // }
  namespace NodeJS {
    interface ProcessEnv {
      MAX_NOT_AUTH_CONVERTS: string;
      MAX_AUTH_CONVERTS: string;
      NOT_AUTH_TIME_LIMIT: string;
      AUTH_TIME_LIMIT: string;
    }
  }
}
