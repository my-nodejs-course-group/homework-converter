import express from 'express';
import session from 'express-session';
import morgan from 'morgan';
import dotenv from 'dotenv';
import cookieParser from 'cookie-parser';
import { routes } from './routes';
import { adjustedPassport, passportInit } from './authStrategy/pasport';
import { isUserExist, createUserInStorage, getUserAccessData } from 'storage';
import { AccessData } from 'storage/types';


dotenv.config();
const app = express();
passportInit();

app.use(cookieParser());
app.use(session({
    secret: <string>process.env.SESSION_SECRET_KEY
}));

app.use(adjustedPassport.initialize());
app.use(adjustedPassport.session());
app.use((req, res, next) => {
  let userId = null;
  if (req.user?.id) {
    userId = req.user?.id;
  } else {
    userId = req.sessionID;
  }
  if (isUserExist(userId)) {
    req.userData = (getUserAccessData(userId) as AccessData);
  } else {
    createUserInStorage(userId, {
      id: userId,
      isAuth: false,
      name: null,
      filesCounter: 0,
      extraConvertsCounter: 0,
      accessToken: null,
      refreshToken: null,
      isPaid: false,
    });
    req.userData = (getUserAccessData(userId) as AccessData);
  }
  next()
});
// app.use((req, res, next) => {
//   if(req.user?.id && isUserExist(req.user?.id)) {
//     req.user =  getUserAccessData(req.user.id)
//   } else if (!isUserExist(req.sessionID)) {
//     createUserInStorage(req.sessionID, {
//         id: req.sessionID,
//         isAuth: false,
//         name: null,
//         filesCounter: Number(process.env.MAX_NOT_AUTH_CONVERTS),
//         accessToken: null,
//         refreshToken: null,
//         isPaid: false,
//     });
//     req.user =  getUserAccessData(req.sessionID);
//   } else {
//     req.user =  getUserAccessData(req.sessionID);
//   }
//   next()
// });

app.use(express.urlencoded());
app.use(morgan('dev'));
app.use(routes);

app.listen(process.env.PORT, () => console.log(`app listening on port: ${process.env.PORT}`));
