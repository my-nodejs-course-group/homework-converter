import { Strategy as GoogleStrategy } from 'passport-google-oauth20';
import jwt from 'jsonwebtoken';
import { getUserAccessData, createUserInStorage, isUserExist } from 'storage';
import passport from 'passport';
import { AccessData } from 'storage/types';

export const passportInit = () => {
  passport.serializeUser((accessData: any, done) => {
    done(null, accessData.id);
  });

  passport.deserializeUser((userId: string, done) => {
    const accessData = getUserAccessData(userId);
    done(null, accessData);
  });

  passport.use(
    new GoogleStrategy(
      {
        clientID: <string>process.env.GOOGLE_CLIENT_ID,
        clientSecret: <string>process.env.GOOGLE_CLIENT_SECRET,
        callbackURL: <string>process.env.GOOGLE_CALLBACK,
        scope: ['profile'],
      },
      (aToken, rToken, profile, done) => {
        if (!isUserExist(profile.id)) {
          const acessTokenTime = '24h';
          const refreshTokenTime = '25h';
          const accessToken = jwt.sign(
            {id: profile.id},
            <jwt.Secret>process.env.JWT_SECRET_KEY,
            {expiresIn: acessTokenTime}
          );
          const refreshToken = jwt.sign(
            {id: profile.id},
            <jwt.Secret>process.env.JWT_SECRET_KEY,
            {expiresIn: refreshTokenTime}
          );

          const accessData: AccessData = {
            id: profile.id,
            isAuth: true,
            name: profile.displayName,
            filesCounter: 0,
            extraConvertsCounter: 0,
            accessToken,
            refreshToken,
            isPaid: false,
          };

          createUserInStorage(profile.id, accessData);
        }
        done(null, getUserAccessData(profile.id));
      }
    )
  );
};

export const adjustedPassport = passport;
