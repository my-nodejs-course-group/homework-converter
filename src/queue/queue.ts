import { QueueItem } from './queue.model';

export class Queue<T> {
  private start: QueueItem<T> | null;
  private end: QueueItem<T> | null;
  private counter: number;

  constructor() {
    this.start = null;
    this.end = null;
    this.counter = 0;
  }

  public push(value: T): number {
    if (this.start && this.end) {
      const newItem = {
        value,
        prev: null,
      };
      this.end.prev = newItem;
      this.end = newItem;
      this.counter++;
      return this.counter;
    } else {
      const newItem = {
        value,
        prev: null,
      };
      this.end = newItem;
      this.start = newItem;
      this.counter++;
      return this.counter;
    }
  }

  public pop(): T | null {
    if (this.counter === 0 || !this.start) {
      return null;
    }

    if (this.counter === 1) {
      const value = this.start.value;
      this.start = null;
      this.end = null;
      this.counter--;
      return value;
    } else {
      const value = this.start.value;
      this.start = this.start.prev;
      this.counter--;
      return value;
    }
  }

  public getSize(): number {
    return this.counter;
  }
}
