export interface QueueItem<T> {
  value: T;
  prev: QueueItem<T> | null;
}
